from django.contrib import admin
from .models import User, Agent, Author, Reviewer, Admin, Editor, ReadingMaterial

# Register your models here.
admin.site.register(User)
admin.site.register(Agent)
admin.site.register(Author)
admin.site.register(Reviewer)
admin.site.register(Admin)
admin.site.register(Editor)
admin.site.register(ReadingMaterial)
