from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
import uuid

# Create your models here.
class User(models.Model):

    #UserRoles
    AGENT_ROLE = "AR"
    REVIEWER_ROLE = "RR"
    AUTHOR_ROLE = "AUR"
    ADMIN_ROLE = "ADM"
    EDITOR_ROLE = "ER"
    ROLE_CHOICES = (
        (AGENT_ROLE, "Agent"),
        (REVIEWER_ROLE, "Reviewer"),
        (AUTHOR_ROLE, "Author"),
        (ADMIN_ROLE, "Admin"),
        (EDITOR_ROLE, "Editor"),
    )

    #Fields
    user_id = models.UUIDField(primary_key = True, default=uuid.uuid4)
    username = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(('password'), max_length=128)
    role = models.CharField(max_length=3, choices=ROLE_CHOICES)
    address = models.CharField(max_length=100)
    fullname = models.CharField(max_length=100)

    #Methods
    def sign_in(self):
        #Allow a user to sign in
        pass

    def check_password(self):
        #Check that the user entered password matches the one stored on the DB
        pass


class Agent(models.Model):

    #Fields
    agent_id = models.UUIDField(primary_key = True, default=uuid.uuid4)
    user_id = models.ForeignKey(User, models.PROTECT)


class Author(models.Model):

    #Fields
    author_id = models.UUIDField(primary_key = True, default=uuid.uuid4)
    user_id = models.ForeignKey(User, models.PROTECT)

    #Methods
    def upload_book(self):
        #Allow the author to upload their book        
        pass

    def update_book(self):
        #Allow the author to update their book
        pass


class Reviewer(models.Model):

    #Fields
    reviewer_id = models.UUIDField(primary_key = True, default=uuid.uuid4)
    user_id = models.ForeignKey(User, models.PROTECT)


class Admin(models.Model):

    #Fields
    admin_id = models.UUIDField(primary_key = True, default=uuid.uuid4)
    user_id = models.ForeignKey(User, models.PROTECT)


class Editor(models.Model):

    #Fields
    editor_id = models.UUIDField(primary_key = True, default=uuid.uuid4)
    user_id = models.ForeignKey(User, models.PROTECT)


class ReadingMaterial(models.Model):

    #Fields
    title = models.CharField(max_length=250, default="book1")
    author_id = models.ForeignKey(Author, models.PROTECT)
    agent_id = models.ForeignKey(Agent, models.PROTECT)
    editor_id = models.ForeignKey(Editor, models.PROTECT)
    revision_number = models.IntegerField()
    published = models.BooleanField()
    reviewer_id = models.ForeignKey(Reviewer, models.PROTECT)
    #reviewer_id = models.ManyToManyField(Reviewer)
    #reviewer_id2 = models.ManyToManyField(Reviewer)
    reviewed = models.BooleanField()
    comments = models.CharField(max_length=250) #Might be problematic to implement. Django officially recommends a 3rd party comment system such as Disqus 
    star_rating = models.IntegerField(default=1, validators=[MaxValueValidator(5), MinValueValidator(1)])

    #Methods
    def publish(self):
        #Publish the book
        ReadingMaterial.published = True

    def review(self):
        #Confirm that the book has passed review
        ReadingMaterial.reviewed = True

    def update(self):
        #Allow a new version of the book to be uploaded
        #New book
        ReadingMaterial.revision_number += 1

    def set_star_rating(self, rating):

        #Set the star rating between 1-5
        ReadingMaterial.star_rating = rating