# Generated by Django 2.2 on 2019-05-30 15:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('exe_publish', '0002_auto_20190530_0947'),
    ]

    operations = [
        migrations.AddField(
            model_name='admin',
            name='user_id',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='exe_publish.User'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agent',
            name='user_id',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='exe_publish.User'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='author',
            name='user_id',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='exe_publish.User'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='editor',
            name='user_id',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='exe_publish.User'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reviewer',
            name='user_id',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='exe_publish.User'),
            preserve_default=False,
        ),
    ]
