from django.shortcuts import render
from exe_publish.models import User, Agent, Author, Reviewer, Admin, Editor, ReadingMaterial

# Create your views here.
def index(request):

    user_role = User.role
    
    if user_role is not "author":
        #This style of snippet would be used to control which type of user could see what page
        pass

    context = {
        'user_role' : user_role
    }

    return render(request, 'index.html', context=context)

def login(request):

    #A view without a template. It would use the form module built into Django
    #If I had created users properly, I could have used Django's pre-built login system

    user_id = User.user_id
    username = User.username
    user_password = User.password

    context = {
        'user_id' : user_id,
        'username' : username,
        'user_password' : user_password
    }

    return render(request, 'login.html', context=context)