from django.apps import AppConfig


class ExePublishConfig(AppConfig):
    name = 'exe_publish'
